


MODELS_PATH = "./models"
COMPANIES = {'indus-valley':True,'nobero':True,'nicobar':True}


INDUS_VALLEY_MODELS_PATH ="./models/indus-valley"
INDUS_VALLEY_DATA_PATH = './data/indus-valley.csv'
INDUS_CHAR_IDX_PATH = "./models/indus-valley/char-inverted-index-indus.pkl"

NOBERO_MODELS_PATH = "./models/nobero"
NOBERO_DATA_PATH = "./data/nobero.csv"
NOBERO_CHAR_IDX_PATH = './models/nobero/char-inverted-index-nobero.pkl'

NICOBAR_MODELS_PATH = "./models/nicobar"
NICOBAR_DATA_PATH = "./data/nicobar.csv"
NICOBAR_CHAR_IDX_PATH = "./models/nicobar/char-inverted-index-nicobar.pkl"


PUNCS = '!"#$&\'()*+,/:;<=>?@[\\]^_`{|}~'
