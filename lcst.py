import random
from locust import task, HttpUser,between


queries = ["?query=I%20want%20a%20travel%20hoodie&company_name=nobero&get_description=false&get_scores=true",
            "?query=I%20want%20a%20travel%20hoodie&company_name=nobero&get_description=true&get_scores=true",
            "?query=I%20want%20a%20travel%20hoodie&company_name=nicobar&get_description=false&get_scores=true",
            "?query=I%20want%20a%20travel%20hoodie&company_name=nicobar&get_description=true&get_scores=true",
            "?query=I%20want%20a%20travel%20hoodie&company_name=indus-valley&get_description=false&get_scores=true",
]

class WebsiteUser(HttpUser):
    # removing wait time for immediate end-points
    wait_time = between(1, 3)
    # wait_time = constant(2) # 2 seconds constant wait time always for all end-points

    @task
    def get_products(self):
        query = random.choice(queries)
        self.client.get('/search-products/'+ str(query))