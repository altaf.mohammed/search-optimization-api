import logging
import functools


# Configuring the logger
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)


def log_to_terminal(func):
    """Decorator to log function calls and results to the terminal."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        """Wrapper function to log function calls and results."""
        logger.debug(f"Calling function {func.__name__} with arguments: {args}, {kwargs}")
        try:
            result = func(*args, **kwargs)
            logger.debug(f"Function {func.__name__} returned: {result}")
            logger.info(f"Function {func.__name__} executed successfully")
            return result
        except Exception as e:
            logger.error(f"Function {func.__name__} raised an exception: {e}")
            raise
    return wrapper