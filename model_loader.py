
import concurrent.futures
from typing import Dict
import pickle 
import os


class ModelsManager:
    models_dict: Dict[str, dict] = {}

    @classmethod
    def load_model(cls,path):
        # go through the given path and load all the models and add them to the models_dict
        for filename in os.listdir(path):
            if filename.endswith('.pickle') or filename.endswith('.pkl'):
                model_name = filename[:-7]
                file_path = os.path.join(path, filename)
                try:
                    with open(file_path, 'rb') as f:
                        loaded_model = pickle.load(f)
                        cls.models_dict[model_name] = loaded_model
                except Exception as e:
                    raise Exception(f"Failed to load model {model_name}: {e} in synchronous loading")


class AsyncModelsManager:
    # Loads the models asynchronously and stores them in a dictionary, a slight optimization than above loader 
    models_dict: Dict[str, dict] = {}

    @classmethod
    def load_model(cls, path):
        def load_and_store_model(filename):
            model_name = filename[:-7]
            file_path = os.path.join(path, filename)
            try:
                with open(file_path, 'rb') as f:
                    loaded_model = pickle.load(f)
                    cls.models_dict[model_name] = loaded_model
            except Exception as e:
                raise Exception(f"Failed to load model {model_name}: {e}")

        try:
            with concurrent.futures.ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
                files = [filename for filename in os.listdir(path) if filename.endswith('.pickle') or filename.endswith('.pkl') and not filename.startswith('char')]
                executor.map(load_and_store_model, files)
        
        except TimeoutError:
            raise Exception("Timed out while loading models, falling back to synchronous loading")
        
        except Exception as e:
            raise Exception(f"Failed to load models: {e}, falling back to synchronous loading")





class IndusValleyModels:
    tfidf_model = 'vectorizer-indus'
    tfidf_docs = 'vectorized-indus-docs'
    bm25_model = 'bm25Okapi-indus'
    char_inv_idx = "char-inverted-index-index"

class NoberoModels:
    tfidf_model = 'nobero-vectorizer'
    tfidf_docs = 'vectorized-nobero-docs'
    bm25_model = 'nobero-bm25Okapi'
    char_inv_idx = "char-inverted-index-nobero"

class NicobarModels:
    tfidf_model = 'nicobar-vectorizer'
    tfidf_docs = 'vectorized-nicobar-docs'
    bm25_model = 'nicobar-bm25Okapi'
    char_inv_idx = "char-inverted-index-nicobar"