from collections import defaultdict
import concurrent.futures
import logging
import csv
import os

from rapidfuzz.fuzz import partial_ratio 
from sklearn.metrics.pairwise import pairwise_distances

from metadata import PUNCS

logging.basicConfig(level=logging.INFO)


def find_relevance(s,values):
    """
    Find the relevance of values in the given string.

    Args:
        s (str): The input string
        values (list): A list of values to check for relevance, ie.., entites extracted from the user query.
    
    Returns:
        float: The relevance score rounded to 3 decimal places
    """
    # we will check how many value exist in the s 
    if not values:
        return 0 
    
    cnt = 0 
    total_values = len(values)
    s = s.lower()
    values = [v[-1].lower().strip() for v in values]
    logging.debug(f"The values are {values} and the query is {s}")
    for value in values:
        score = partial_ratio(s,value)
        if score >= 90:
            cnt += 1 
    return round(cnt/total_values,3)


def tfidf_values(df,vectorized_query,duplicates,entites,tfidf_docs,get_description=False):
    """
    Calculate the TF-IDF values for the given query and return the top 10 results with their scores and relevance scores.

    Args:
        df: The dataframe containing the document information.
        vectorized_query: The vectorized form of the query.
        duplicates: A dictionary to store duplicate document ids.
        entites: The entities to consider for relevance scoring.
        tfidf_docs: The TF-IDF vectors of the documents.

    Returns:
        A dictionary containing the scores, results, and relevance scores of the top 10 TF-IDF results.
    """
    logging.info(" Getting the Tf-Idf results")
    
    # tfidf results 
    tf_idf_results = {'scores':[],'result':[],'relevance':[]}
    descp = []
    
    pairwise_dist = pairwise_distances(vectorized_query,tfidf_docs,metric='cosine')
    tfidf_scores = (1 - pairwise_dist)[0]
    top_n_res = tfidf_scores.argsort()[::-1][:10]

    for idx in top_n_res:
        
        id, name,entity_descp = df[idx+1][0] , df[idx+1][1], df[idx+1][2]
        logging.debug(f"the id is {id} and the name is {name}")
        score = tfidf_scores[idx]
        relevance_score = find_relevance(entity_descp,entites)
        duplicates[id] = True
        
        tf_idf_results['scores'].append(round(score,3))
        tf_idf_results['result'].append(name)
        tf_idf_results['relevance'].append(relevance_score)

        if get_description:
            try:
                description = df[idx+1][3] 
                descp.append(description)
            except:
                pass
    
    if descp is not None:
        tf_idf_results['description'] = descp
    
    return tf_idf_results


def bm25_values(df,tokenized_query,duplicates,entites,bm25_model,get_description=False):
    """
    Calculate BM25 scores for a tokenized query and return the top 10 results with relevance scores.
    
    Args:
        df: The dataframe containing the relevant data.
        tokenized_query: The query tokenized into individual terms.
        duplicates: A dictionary to track duplicate results.
        entites: The entities to be matched with the results.
        bm25_model: The BM25 model used for scoring.
        
    Returns:
        bm25_results: A dictionary containing scores, results, and relevance scores for the top matches.
    """
    logging.info(" Getting the bm25 results")
    bm25_results = {'scores':[],'result':[],'relevance':[]}
    descp = []
    scores = bm25_model.get_scores(tokenized_query)
    top_n_scores = scores.argsort()[::-1][:10]

    for idx in top_n_scores:

        id, name,entity_descp = df[idx+1][0] , df[idx+1][1], df[idx+1][2]
        logging.debug(f"the id is {id} and the name is {name}")
        if not duplicates[id]:
            duplicates[id] = True     
            relevance_score = find_relevance(entity_descp,entites)
            bm25_results['scores'].append(round(scores[idx],3))
            bm25_results['result'].append(name)
            bm25_results['relevance'].append(relevance_score)
            if get_description:
                try:
                    description = df[idx+1][3] 
                    descp.append(description)
                except:
                    pass
    # include the description if we have computed it
    if descp:
        bm25_results['description'] = descp
    
    return bm25_results




def jsonify(produtcs_data, get_scores = False,get_description=False):
    """
    Creates a JSON like format from the given products data.

    Args:
        produtcs_data (dict): A dictionary containing products data.
        get_scores (bool, optional): Whether to include scores in the JSON. 
            Defaults to False.

    Returns:
        dict: A JSON like formatted object containing the results.
    """
    logging.info("jsonifying the total data")

    json_data = {'results':[]}
    for key, inner_dict in produtcs_data.items():
        scores = inner_dict['scores']
        relevance = inner_dict['relevance']
        result = inner_dict['result']

        for i in range(len(relevance)):
            if relevance[i] > 0:
                data = {}
                data['name'] = result[i]
                data['relevance'] = relevance[i]
                if get_scores and scores[i] > 0:
                    if key == 'tf_idf':
                        data['tf_idf_score'] = scores[i] 
                    else:
                        data['bm25_score'] = scores[i]
                if get_description:
                    try:
                        data['description'] = inner_dict['description'][i]
                    except:
                        pass 

                json_data['results'].append(data)
    
    # sortint the results based upon the highest relevance
    inner_data = json_data['results']
    json_data['results'] = sorted(inner_data,key=lambda x: x['relevance'],reverse=True)
    return json_data



def get_responses_concurrent(query, entites, 
                             tfidf_vectorizer, tfidf_docs, bm25_model, 
                             data_path,get_description,get_scores):
    """
    Get responses using TF-IDF and BM25 models.

    Args:
        query (str): The input query string.
        entites (list): List of entities to consider.
        tfidf_vectorizer (object): The TF-IDF vectorizer object.
        tfidf_docs (list): List of TF-IDF documents.
        bm25_model (object): The BM25 model object.
        data_path (str): The path to the data file.

    Returns:
        dict: A dictionary containing TF-IDF and BM25 results, formatted as JSON.
    """
    logging.info("finding all the responses")

    duplicates = defaultdict(bool)
    query = query.lower().translate(str.maketrans("", "", PUNCS))
    logging.debug(f"The transformed query is --> {query}")
    tokenized_query = query.split()
    vectorized_query = tfidf_vectorizer.transform([query]).astype('float32')
    tfidf_docs = tfidf_docs.astype('float32')
    res = {}

    # Parallelize the function calls
    with concurrent.futures.ProcessPoolExecutor(max_workers=os.cpu_count()) as executor, open(data_path,'r',encoding='utf-8') as file:
        df = csv.reader(file)
        df = list(df)
        tfidf_future = executor.submit(tfidf_values, df, vectorized_query, duplicates, entites, tfidf_docs,get_description)
        bm25_future = executor.submit(bm25_values, df, tokenized_query, duplicates, entites, bm25_model,get_description)

    # Get the results
    res['tf_idf'] = tfidf_future.result()
    res['bm25'] = bm25_future.result()
    
    logging.info("done getting results")

    json_result = jsonify(res, get_scores=get_scores,
                          get_description=get_description)
    return json_result
