


from model_loader import AsyncModelsManager
from metadata import (COMPANIES, 
                      INDUS_VALLEY_MODELS_PATH, INDUS_VALLEY_DATA_PATH, 
                      NOBERO_MODELS_PATH, NOBERO_DATA_PATH,
                      NICOBAR_MODELS_PATH, NICOBAR_DATA_PATH)
from products_extraction import get_responses_concurrent
from model_loader import IndusValleyModels, NoberoModels, NicobarModels

class LoadModels:
    def __init__(self,company_name):
        if COMPANIES[company_name]:
            if company_name == 'indus-valley':
                self.models_manager = self.load_models(INDUS_VALLEY_MODELS_PATH)
            if company_name == "nobero":
                self.models_manager = self.load_models(NOBERO_MODELS_PATH)
            if company_name == 'nicobar':
                self.models_manager = self.load_models(NICOBAR_MODELS_PATH)
    
    def load_models(self,path):
        model_manager = AsyncModelsManager()
        model_manager.load_model(path) # all the models path for that specific company
        return model_manager

    

class GetProducts:
    def __init__(self,company_name):
        self.company_name = company_name
        self.loaded_models = LoadModels(company_name).models_manager
    
    def get_products(self,query,entities,get_description,get_scores):
        if self.company_name == 'indus-valley':
            products = get_responses_concurrent(query,entities,
                                self.loaded_models.models_dict[IndusValleyModels.tfidf_model],
                                self.loaded_models.models_dict[IndusValleyModels.tfidf_docs],
                                self.loaded_models.models_dict[IndusValleyModels.bm25_model],
                                INDUS_VALLEY_DATA_PATH,
                                get_description,get_scores)
        elif self.company_name == 'nobero':
            products = get_responses_concurrent(query,entities,
                                        self.loaded_models.models_dict[NoberoModels.tfidf_model],
                                        self.loaded_models.models_dict[NoberoModels.tfidf_docs],
                                        self.loaded_models.models_dict[NoberoModels.bm25_model],
                                        NOBERO_DATA_PATH,
                                        get_description,get_scores)
        elif self.company_name == 'nicobar':
            products = get_responses_concurrent(query,entities,
                                    self.loaded_models.models_dict[NicobarModels.tfidf_model],
                                    self.loaded_models.models_dict[NicobarModels.tfidf_docs],
                                    self.loaded_models.models_dict[NicobarModels.bm25_model],
                                    NICOBAR_DATA_PATH,
                                    get_description,get_scores)
        
        else:
            products = None

        return products