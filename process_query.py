import logging
import string

from rapidfuzz.fuzz import partial_ratio

from metadata import PUNCS

logging.basicConfig(level=logging.INFO)

def find_entites_token_level_spans(query,stopwords_lookup, char_inverted_index):
    """
    Find entities in the given query using the provided stopwords lookup and character inverted index.
    
    Args:
        query (str): The input query string.
        stopwords_lookup (dict): A dictionary lookup for identifying stopwords.
        char_inverted_index(dict[list]): The character inverted index, with character level products split for efficient entity extraction.
    
    Returns:
        list[tuple]: A list of tuples containing the position and the original token of the extracted entities.
    """
    logging.info(f" query --> {query}")
    query = query.strip().translate(str.maketrans("", "", PUNCS))
    original_query = query.split()
    tokenized_query = query.lower().split()

    # extract all the entites from the query
    tokens_lst = [] # (token,position)
    for idx, token in enumerate(tokenized_query):
        logging.debug(f"Token --> {token}")

        if stopwords_lookup[str(token)]:
            continue 
            
        check_lst = char_inverted_index[token[0]].keys()
        logging.debug(f"the check list is --> {check_lst}")
        for check in check_lst:
            score = partial_ratio(token,check)
            if score >= 80:
                tokens_lst.append((idx,original_query[idx]))
                break
    
    
    return tokens_lst



def find_entities_char_level_spans(query, stopwords_lookup, char_inverted_index):
    """
    Find entities in the given query using the provided stopwords lookup and character inverted index.
    
    Args:
        query (str): The input query string.
        stopwords_lookup (dict): A dictionary lookup for identifying stopwords.
        char_inverted_index (dict[list]): The character inverted index, with character level products split for efficient entity extraction.
    
    Returns:
        list[tuple]: A list of tuples containing the positions and the original token of the extracted entities.
    """
    # Remove punctuation and split query into characters
    query = query.strip().translate(str.maketrans("", "", PUNCS))
    
    # Initialize list to store extracted entities
    entities = []

    # Iterate over each character of the query
    idx = 0
    while idx < len(query):
        # Find next non-stopword character
        while idx < len(query) and query[idx] in string.whitespace + PUNCS:
            idx += 1
        
        # Start index of the current entity
        start_idx = idx
        
        # Find the end of the current entity
        while idx < len(query) and query[idx] not in string.whitespace + PUNCS:
            idx += 1
        
        # End index of the current entity
        end_idx = idx
        
        # Extract the current entity
        entity = query[start_idx:end_idx]
        
        # Check if the entity is a stopword
        if stopwords_lookup.get(entity.lower(), False):
            continue
        
        # Check if the entity matches any key in the character inverted index
        check_lst = char_inverted_index[entity[0]]
        for check in check_lst:
            score = partial_ratio(entity, check)
            if score >= 80:
                entities.append((start_idx,start_idx+len(entity), entity))
                break
    
    return entities
