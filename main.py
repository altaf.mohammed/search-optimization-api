import os
import pickle
import logging

from contextlib import asynccontextmanager
from fastapi import FastAPI, Query
from fastapi.responses import ORJSONResponse, RedirectResponse
from fastapi.middleware.gzip import GZipMiddleware
from pydantic import BaseModel 
from typing import Optional

from process_query import find_entities_char_level_spans
from utils import load_pickle_file
from metadata import( MODELS_PATH, INDUS_CHAR_IDX_PATH,
                     NOBERO_CHAR_IDX_PATH, NICOBAR_CHAR_IDX_PATH)
from assembler import GetProducts

logging.basicConfig(level=logging.INFO)


class UtilsRegistery(BaseModel):
    utils_dict: dict

# utility models used throughout the script
class UtilsNames:
    stopwords_lookup: str = 'stopwords_lookup' 


def load_utils(utils_path: str):
    logging.info("Starting to load all the utilities")
    models = {}
    for filename in os.listdir(utils_path):
        if filename.endswith(".pkl"):
            file_path = os.path.join(utils_path, filename)
            with open(file_path, "rb") as file:
                try:
                    logging.debug(f"{filename}")
                    model_name = filename[:-4]  # Remove the .pkl extension
                    models[model_name] = pickle.load(file)
                
                except Exception as e:
                    raise Exception(f"There was erroe {e} when loading the file {filename}")
    logging.info(f"loaded a total of {len(models)} models ")
    return models

@asynccontextmanager
async def lifespan(app: FastAPI):
    # lifespan is a coroutine that runs when the application starts, and closes when the application stops
    utils = load_utils(MODELS_PATH)
    UtilsRegistery.utils_dict = utils

    logging.info(f"{UtilsRegistery.utils_dict.keys()} \n")
    yield

    # Clean up the models and release the resources
    utils.clear()


app = FastAPI(lifespan=lifespan,title="LimeChat Product Search Engine")
app.add_middleware(GZipMiddleware, minimum_size=2000)

def verify_query(query,stopwords_lookup,char_inverted_index):
    query = query.strip()
    if query == '':
        return 'empty'

    entites = find_entities_char_level_spans(query,stopwords_lookup,char_inverted_index)
    return entites




@app.get('/search-products/')
async def search_products(
    query: str = Query(...,title='query'),
    company_name: str = Query('indus-valley',alias='company_name',description="Enter the name in lowercase with hyphen for spaces. Example: indus-valley"),
    get_description: Optional[bool] = Query(False, alias="get_description"),
    get_scores: Optional[bool] = Query(True, alias="get_scores")

):
    # end-point to return products from the specified company 
    response_json = {}
    query = query.strip()
    
    stopwords_lookup = UtilsRegistery.utils_dict[UtilsNames.stopwords_lookup]
    if company_name == 'indus-valley':
        char_inverted_index = load_pickle_file(INDUS_CHAR_IDX_PATH)
    elif company_name == 'nobero':
        char_inverted_index = load_pickle_file(NOBERO_CHAR_IDX_PATH)
    elif company_name == 'nicobar':
        char_inverted_index = load_pickle_file(NICOBAR_CHAR_IDX_PATH)
    else:
        return ORJSONResponse(content="Company name not found")
    
    entites = verify_query(query,stopwords_lookup,char_inverted_index)
    if entites == 'empty':
        return ORJSONResponse(content="Empty Query")
    
    logging.debug(f" Extracted Entites from the user query are ---> {entites}")
    response_json['query'] = query  # adding query to our response object
    
    if not entites:
        return ORJSONResponse(content={'response':"There is no matching product found based upon your query"})
    logging.info("sucessfully retreived the entites")

    # adding to the output json if found entites in the query 
    response_json['entites'] = entites

    # load the required models for finding the products
    get_products = GetProducts(company_name)
    products = get_products.get_products(query,entites,get_description,get_scores)
    logging.info(f"got {len(products['results'])} products")
    
    response_json['results'] = products['results']

    if get_description:
        if response_json['results'] and 'description' not in response_json['results'][0].keys():
            response_json['description'] = f"Description is not available for the company {company_name}"


    return ORJSONResponse(content = response_json)
    





"""
Redirecting the home-page to the FastAPI docs on-Startup

"""

@app.get("/")
def redirect_to_docs():
    return RedirectResponse("/docs", status_code=307)
